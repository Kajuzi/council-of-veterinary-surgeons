<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return redirect( '/members/' . \Auth::user()->id );
});

Auth::routes();

Route::resource('members', 'MembersController');
Route::resource('subscription', 'SubscriptionController');
Route::resource('transactions', 'TransactionController');
Route::resource('legislature', 'RegulationsController');

Route::get('about/organisation', function () {
    return redirect('about');
});

Route::get('about', 'PagesController@about');
Route::post('contact', 'PagesController@sendmail');
Route::post('find', 'MembersController@search');
Route::post('search', 'PagesController@search');

// If the route does not exist but the view does display the view
Route::get('{view}', function($view){
    if(file_exists(config('view.paths')[0] . "/$view.blade.php"))
        return view(str_replace('/', '.', $view));
    else
        abort(404);
});

Route::get('{parent}/{view}', function($parent, $view){
    if(file_exists(config('view.paths')[0] . "/$parent/$view.blade.php"))
        return view("$parent.$view");
    else
        abort(404);
});