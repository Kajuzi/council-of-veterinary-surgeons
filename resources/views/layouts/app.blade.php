<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Title -->
    <title>@yield('page_title')</title>
    <!-- Favicon -->
    <link href="{{ asset('favicon.ico') }}" sizes="16x16" rel="shortcut icon" />
    <link href="{{ asset('images/favicon.ico') }}" sizes="128x128" rel="shortcut icon" type="image/x-icon" />
    <!-- css file -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/theme-color.css') }}">
    <link rel="stylesheet" href="{{ asset('css/boxed.css') }}">
    <!-- Responsive stylesheet -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    @yield('head_scripts')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="wrapper">
	<div id="preloader" class="preloader">
		<div id="pre" class="preloader_container">
		</div>
	</div>
  	<div class="ulockd-header-topped">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-6">
  					<div class="ulockd-social-linked">
  						<p class="ulockd-welcntxt">Phone<span class="text-thm1"> <a href="tel:+263775471642">+263 775 471 642</a></span></p>
  					</div>
  				</div>
  				<!-- <div class="col-md-4">
  					<div class="ulockd-welcm-ht text-center">
	  					<p class="ulockd-welcntxt"> Flash message here  </p>
  					</div>
  				</div> -->
  				<div class="col-md-6">
  					<div class="ulockd-welcm-ht text-right">
						<ul>
							@guest
								<li class="nav-item">
									<a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
								</li>
								@if (Route::has('register'))
									<li class="nav-item">
										<a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
									</li>
								@endif
							@else
								<li class="nav-item dropdown">
									<a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
										{{ Auth::user()->name }} <span class="caret"></span>
									</a>

									<div class="dropdown-menu dropdown-menu-right logout-link" aria-labelledby="navbarDropdown">
										<a class="dropdown-item" href="{{ route('logout') }}"
										onclick="event.preventDefault();
														document.getElementById('logout-form').submit();">
											{{ __('Logout') }}
										</a>

										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											@csrf
										</form>
									</div>
								</li>
							@endguest							
						</ul>

  					</div>
  				</div>
  			</div>
  		</div>
  	</div>
  	<div class="ulockd-header-middle">
  		<div class="container">
  			<div class="row">
  				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="ulockd-contact-info text-right">
						<div class="ulockd-icon pull-right"><i class="fa fa-search"></i></div>
							<a href="{{ url('members') }}">
								<div class="ulockd-info">
									<h3>Find</h3>
									<p class="ulockd-cell">members</p>
								</div>
							</a>
					</div>
  				</div>
  				<div class="col-xs-12 col-sm-4 col-md-4">
  					<div class="ulockd-welcm-hmddl text-center">
						<a href="{{ url('/') }}" class="ulockd-factr-main-logo"><img src="{{ asset('images/cvsz-logo.png') }}" alt=""></a>
  					</div>  					
  				</div>
  				<div class="col-xs-12 col-sm-4 col-md-4">
					<div class="ulockd-ohour-info">
						<div class="ulockd-icon pull-left"><span class="flaticon-clock-1"></span></div>
						<div class="ulockd-info">
							<h3>Business Hours</h3>
							<p class="ulockd-addrss">Mon - Fri 09:00-16:00</p>
						</div>
					</div>
  				</div>
  			</div>
  		</div>
  	</div>

	<!-- Header Styles -->
	<header class="header-nav">
		<div class="main-header-nav navbar-scrolltofixed">
			<div class="container">
			    <nav class="navbar navbar-default bootsnav ulockd-menu-style-one light-red">
					<!-- Start Top Search -->
			        <div class="top-search">
			            <div class="container">
			                <div class="input-group">
			                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
			                    <input type="text" class="form-control" placeholder="Search">
			                    <span class="input-group-addon close-search"><i class="fa fa-times"></i></span>
			                </div>
			            </div>
			        </div>
			        <!-- End Top Search -->

			        <div class="container ulockd-pad-90">
			            <!-- Start Atribute Navigation -->
			            <div class="attr-nav">
			                <ul>
			                    <li class="search"><a href="#"><i class="fa fa-search"></i></a></li>
			                    <li class="side-menu"><a href="#"><i class="fa fa-bars"></i></a></li>
			                </ul>
			            </div>
			            <!-- End Atribute Navigation -->

			            <!-- Start Header Navigation -->
			            <div class="navbar-header">
			                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
			                    <i class="fa fa-bars"></i>
			                </button>
			            </div>
			            <!-- End Header Navigation -->

			            <!-- Collect the nav links, forms, and other content for toggling -->
			            <div class="collapse navbar-collapse" id="navbar-menu">
			                <ul class="nav navbar-nav navbar-left" data-in="bounceIn">
			                    <li class="dropdown">
			                        <a href="{{ url('/') }}" class="">Home</a>
			                    </li>
			                    <li class="dropdown">
			                        <a href="{{ url('about') }}" class="dropdown-toggle" data-toggle="dropdown">About</a>
			                        <ul class="dropdown-menu">
										<li><a href="{{ url('about/organisation') }}">Organisation</a></li>
										<li><a href="{{ url('about/councilors') }}">Councilors</a></li>
										<li><a href="{{ url('about/secretariat') }}">Secretariat</a></li>
										<li><a href="{{ url('about/jurisdiction') }}">Jurisdiction</a></li>
										<li><a href="{{ url('about/fees') }}">Schedule of Fees</a></li>
										<li><a href="{{ url('about/annual-reports') }}">Annual Reports</a></li>
			                        </ul>
			                    </li>
			                    <li class="members">
			                        <a href="{{ url('members') }}" class="">Members</a>
			                    </li>
			                    <li class="dropdown">
			                        <a href="{{ url('legislature') }}" class="">Legislature</a>
			                    </li>
			                    <li class="dropdown">
			                        <a href="{{ url('blog') }}" class="">Blog</a>
			                    </li>
			                    <li class="dropdown">
			                        <a href="{{ url('faq') }}" class="">FAQ</a>
			                    </li>
			                    <li class="dropdown">
			                        <a href="{{ url('contact') }}" class="">Contact</a>
			                    </li>
			                </ul>
			            </div><!-- /.navbar-collapse -->
			        </div>

			        <!-- Start Side Menu -->
			        <div class="side">
			            <a href="#" class="close-side"><i class="fa fa-times"></i></a>
			            <div class="widget">
			                <h4 class="title">Pages</h4>
			                <ul class="link">
			                    <li><a href="{{ url('about') }}">About</a></li>
			                    <li><a href="{{ url('blog') }}">Blog</a></li>
			                    <li><a href="{{ url('contact') }}">Contact</a></li>
			                </ul>
			            </div>
			            <div class="widget">
			                <h4 class="title">Useful Links</h4>
			                <ul class="link">
			                    <li><a href="#">Link 1</a></li>
			                    <li><a href="#">Link 2</a></li>
			                    <li><a href="#">Link 3</a></li>
			                </ul>
			            </div>
			        </div>
			        <!-- End Side Menu -->
			        
			    </nav>
			</div>
		</div>		
	</header>

	@if(has_header())
	<!-- Home Design Inner Pages -->
	<div class="ulockd-inner-home" style="background-image: url({{ title_bg( $view_name ) }}); background-size: cover;background-position: left;">
		<div class="container text-center">
			<div class="row">
				<div class="ulockd-inner-conraimer-details">
					<div class="col-md-12">
						<h1 class="text-uppercase">@yield('page_title')</h1>
					</div>
					<div class="col-md-12">
						<div class="ulockd-icd-layer">
							<ul class="list-inline ulockd-icd-sub-menu">
								<li><a href="#"> HOME </a></li>
								<li><a href="#"> > </a></li>
								<li><a href="#"> @yield('page_title') </a> </li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif

    @yield('content')

	<!-- Our Footer -->
	<section class="ulockd-footer">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
					<div class="ulockd-footer-qlink">
						<h3 class="text-uppercase">LOCA<span class="text-thm1">TION</span></h3>
						<p>
							Council Offices 10F13 and 10F14<br>
							Faculty of Veterinary Science<br>
							University of Zimbabwe<br>
							630 Churchill Avenue<br>
							Mt Pleasant<br>
							Harare<br>
							Zimbabwe
						</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
					<div class="ulockd-footer-contact">
						<h3 class="text-uppercase">CONTACT <span class="text-thm1">US</span></h3>
						<p>You can either use the phone numbers below or simply email us using the form on the <a href="{{ url('contact') }}">contact page</a></p>
						<div class="ulockd-ftr-phone"><span>Mobile </span>: <a href="tel:+263775471642"> +263 775 471 642</a></div>
						<div class="ulockd-ftr-phone"><span>Mobile </span>: <a href="tel:+263713668583"> +263 713 668 583</a></div>
						<div class="ulockd-ftr-phone"><span>Mobile </span>: <a href="tel:+263735393918"> +263 735 393 918</a></div>
						<div class="ulockd-ftr-phone"><span>Phone </span>: <a href="tel:+2638644109886"> +263 864 410 9886</a></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
					<div class="ulockd-footer-qlink">
						<h3 class="text-uppercase">USEFUL <span class="text-thm1">LINKS</span></h3>
						<ul class="list-unstyled list-two-columns">
							<li><a href="#">Home</a></li>
							<li><a href="#">Our Services</a></li>
							<li><a href="#">About Us</a></li>
							<li><a href="#">Team Details</a></li>
							<li><a href="#">News</a></li>
							<li><a href="#">Contact Us</a></li>
						</ul>
					</div>				
				</div>
			</div>
		</div>
	</section>

	<!-- Our CopyRight Section -->
	<section class="ulockd-copy-right">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="color-white">CVSZ Copyright © 2019. All right reserved. Site by <a class="color-white" href="https://kajuzi.com/ref/cvsz" target="_blank">Kajuzi</a></p>
				</div>
			</div>
		</div>
	</section>

<a class="scrollToTop" href="#"><i class="fa fa-home"></i></a>
</div>
<!-- Wrapper End -->
<script type="text/javascript" src="{{ asset('js/jquery-1.12.4.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootsnav.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/scrollto.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-scrolltofixed-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.counterup.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fancybox.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.masonry.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fitvids.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/timepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/tweetie.js') }}"></script>
<!-- Custom script for all pages --> 
<script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
@yield('footer_scripts')

</body>
</html>
