@extends('layouts.app')

@section('page_title', 'Login')

@section('content')

	<!-- Form Section -->
	<section class="ulockd-about-one">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-6 col-md-offset-3">
					<div class="booking_form_style1 text-center">
		                <!-- Login Form Start-->
			            <form id="general-form" class="general-form text-center" name="login_form" action="{{ route('login') }}" method="post" novalidate="novalidate">
                            @csrf
			                <div class="messages"></div>
							<div class="row">
				                <div class="col-sm-12">
				                	<h3>@yield('page_title')</h3>
				                </div>
				                <div class="col-sm-12">
				                    <div class="form-group">
                                        <input id="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" required="required" data-error="Name is required." type="text" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
										<div class="help-block with-errors">
                                            {{ $message }}
                                        </div>
                                        @enderror
									</div>
				                </div>
				                <div class="col-sm-12">
				                    <div class="form-group">
				                    	<input id="password" name="password" class="form-control required password" placeholder="password" required="required" data-error="Password is required." type="password">
										@error('username')<div class="help-block with-errors">{{ $message }}</div>@enderror
				                    </div>
				                </div>
				                <div class="col-sm-12">
				                    <div class="form-group">
					                    <input name="form-botcheck" class="form-control" type="hidden" value="">
					                    <button type="submit" class="btn ulockd-btn-thm2 btn-block ">Login</button>
				                    </div>
				                </div>
				                <div class="col-md-7">
				                    <div class="form-group">
				                    	<div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
				                    </div>
				                </div>
				                <div class="col-md-5">
				                    <div class="form-group">
				                    	@if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
				                    </div>
				                </div>
			                </div>
			            </form>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
