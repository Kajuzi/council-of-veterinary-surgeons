@extends('layouts.app')
@section('page_title', 'Legislature')
@section('content')

	<!-- Our About Page faq Section -->
	<section class="ulockd-ap-faq">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
              	   <div class="ulockd-faq-box">
                		<div class="ulockd-ap-faq-title clearfix">
							<h2></h2>
						</div>
	                	<div class="ulockd-faq-content">
	                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								@foreach($regulations as $reg)
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="heading_{{ str_pad($reg->id, 3, '0', STR_PAD_LEFT) }}">
			                            <h4 class="panel-title">
			                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{ str_pad($reg->id, 3, '0', STR_PAD_LEFT) }}" aria-expanded="false" aria-controls="collapse_{{ str_pad($reg->id, 3, '0', STR_PAD_LEFT) }}">
			                                <i class="fa fa-angle-down icon-1"></i>
			                                <i class="fa fa-angle-up icon-2"></i>
			                                {{ $reg->title }} - {{ $reg->reference }} 
			                                </a>
			                            </h4>
			                        </div>
			                        <div id="collapse_{{ str_pad($reg->id, 3, '0', STR_PAD_LEFT) }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{ str_pad($reg->id, 3, '0', STR_PAD_LEFT) }}">
			                            <div class="panel-body">
			                              {{ $reg->provisions }}
			                            </div>
			                        </div>
								</div>
								@endforeach
	                        </div>
	                	</div>
              	   </div>
				</div>
				<!-- <div class="col-xs-12 col-sm-12 col-md-7">
					<div class="ulockd-faq-thumb">
						<img class="img-responsive img-whp" src="https://via.placeholder.com/670x400/2da22c?text=FAQ+Placeholder+image" alt="faq.jpg">
					</div>
				</div> -->
			</div>
		</div>
	</section>
@endsection