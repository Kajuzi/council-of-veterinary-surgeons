@extends('layouts.app')
@section('page_title', 'Home')
@section('content')

	<!-- Home Design -->
	<div class="ulockd-home-slider">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 ulockd-pmz">
					<div class="ulockd-main-slider">
						<div class="item">
							<div class="caption">
								<div class="ulockd-text-one wow fadeInUp" data-wow-duration="300ms" data-wow-delay=".3s">Improvement of <span class="text-thm1">veterinary services</span></div>
								<div class="ulockd-text-two wow fadeInUp" data-wow-duration="600ms" data-wow-delay=".6s">in Zimbabwe</div>
								<div class="ulockd-text-three wow fadeInUp" data-wow-duration="900ms" data-wow-delay=".9s">
									<p>Some blurb about how you sit amet consectetur adipisicing elit. Debitis praesentium hic labore tenetur, ipsum non nihil aliquam placeat.</p>
								</div>
								<a href="{{ url('about') }}" class="btn btn-lg ulockd-btn-thm ulockd-home-btn wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="1.2s"><span> Find Out More</span></a>
							</div>
							<img class="img-responsive img-whp" src="images/home/chihuahua-home-slider.jpg" alt="chihuahua-home-slider.jpg">
						</div>						
						<div class="item">
							<div class="caption">
								<div class="ulockd-text-one wow fadeInUp" data-wow-duration="300ms" data-wow-delay=".3s">Improvement of <span class="text-thm1">veterinary services</span></div>
								<div class="ulockd-text-two wow fadeInUp" data-wow-duration="600ms" data-wow-delay=".6s">in Zimbabwe</div>
								<div class="ulockd-text-three wow fadeInUp" data-wow-duration="900ms" data-wow-delay=".9s">
									<p>Some blurb about how you sit amet consectetur adipisicing elit. Debitis praesentium hic labore tenetur, ipsum non nihil aliquam placeat.</p>
								</div>
								<a href="{{ url('about') }}" class="btn btn-lg ulockd-btn-thm ulockd-home-btn wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="1.2s"><span> Find Out More</span></a>
							</div>
							<img class="img-responsive img-whp" src="images/home/cow-1715829_1366.jpg" alt="cow-1715829_1366.jpg">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Our Services -->
	<section class="ulockd-service-one">
		<div class="container">
			<div class="row">
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3 col-lg-3 ulockd-srvcbg">
					<div class="ulockd-service-fstcol">
						<div class="ulockd-srvc-icon text-thm1"><i class="fa fa-check" aria-hidden="true"></i></span></div>
						<h3> CPD<br>&nbsp;</h3>
						<p>The council is accredited to carry out Continuing Professional Development (CPD) Accreditation</p>
						<p><a href="{{ url('about') }}">Find out more</a></p>
					</div>
				</div>
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3 col-lg-3 ulockd-srvcbg">
					<div class="ulockd-service-fstcol">
						<div class="ulockd-srvc-icon text-thm1"><i class="fa fa-newspaper-o" aria-hidden="true"></i></span></div>
						<h3> Continual Improvement</h3>
						<p>We are always equiping our members with latest and relevant information regarding the industry</p>
						<p><a href="{{ url('about') }}">Find out more</a></p>
					</div>
				</div>
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3 col-lg-3 ulockd-srvcbg">
					<div class="ulockd-service-fstcol">
						<div class="ulockd-srvc-icon text-thm1"><i class="fa fa-list" aria-hidden="true"></i></div>
						<h3> Industry<br>Regulation</h3>
						<p>Corporis fuga ipsa sapiente voluptate adipisci natus tempore aut commodi illum necessitatibus.</p>
						<p><a href="{{ url('about') }}">Find out more</a></p>
					</div>
				</div>
				<div class="col-xxs-12 col-xs-6 col-sm-6 col-md-3 col-lg-3 ulockd-srvcbg">
					<div class="ulockd-service-fstcol">
						<div class="ulockd-srvc-icon text-thm1"><i class="fa fa-comments-o" aria-hidden="true"></i></div>
						<h3> Foster<br>Dialogue</h3>
						<p>Libero saepe minima obcaecati reprehenderit, accusantium autem sit officia exercitationem pariatur asperiores excepturi.</p>
						<p><a href="{{ url('about') }}">Find out more</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Our About -->
	<section class="ulockd-about-one">
		<div class="container">
			<div class="row home-about-section">
				<div class="col-sm-12 col-md-7 col-lg-7 ulockd-abtonspc">
					<div class="ulockd-about-ondetials">
						<h2>About <span class="text-thm1"> CVSZ</span></h2>
						<p>The Council for Veterinary Surgeons of Zimbabwe is a  beatae amet ratione repudiandae impedit nam eius, hic necessitatibus natus, porro esse? Incidunt consectetur tenetur vel enim, ratione reprehenderit?</p>
					</div>
				</div>
				<div class="col-sm-12 col-md-5 col-lg-5">
					<div class="ulockd-about-thumb">
						<img class="img-responsive img-whp" src="{{ asset('images/goat-home.jpg') }}" alt="Curious Cow">
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection
