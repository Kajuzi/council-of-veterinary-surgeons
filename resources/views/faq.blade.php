@extends('layouts.app')

@section('page_title', 'FAQ')

@section('content')

	<!-- Our About Page faq Section -->
	<section class="ulockd-ap-faq">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-5">
              	   <div class="ulockd-faq-box">
                		<div class="ulockd-ap-faq-title clearfix">
							<h2>Frequently Asked  <span class="text-thm1">Questions</span></h2>
						</div>
	                	<div class="ulockd-faq-content">
	                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="headingOne">
			                            <h4 class="panel-title">
			                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
			                              <i class="fa fa-angle-down icon-1"></i>
			                              <i class="fa fa-angle-up icon-2"></i>
			                              All the Lorem Ipsum generators
			                                </a>
			                            </h4>
			                        </div>
			                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
			                            <div class="panel-body">
			                              <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,</p>
			                            </div>
			                        </div>
		                        </div>
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="headingTwo">
			                            <h4 class="panel-title">
			                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
			                                <i class="fa fa-angle-down icon-1"></i>
			                                <i class="fa fa-angle-up icon-2"></i>
			                                Reader will be distracted by the 
			                                </a>
			                            </h4>
			                        </div>
			                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
			                            <div class="panel-body">
			                              <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,</p>
			                            </div>
			                        </div>
		                        </div>
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="headingThree">
			                            <h4 class="panel-title">
				                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
					                            <i class="fa fa-angle-down icon-1"></i>
					                            <i class="fa fa-angle-up icon-2"></i>
				                                Amar Sonar Bangal ami tomay
				                            </a>
			                            </h4>
			                        </div>
			                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
			                            <div class="panel-body">
			                              <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,</p>
			                            </div>
			                        </div>
		                        </div>
		                        <div class="panel panel-default">
			                        <div class="panel-heading" role="tab" id="headignFour">
			                            <h4 class="panel-title">
				                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
					                            <i class="fa fa-angle-down icon-1"></i>
					                            <i class="fa fa-angle-up icon-2"></i>
				                                valobashi chirodin tomar akash
				                            </a>
			                            </h4>
			                        </div>
			                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headignFour">
			                            <div class="panel-body">
			                              <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,</p>
			                            </div>
			                        </div>
		                        </div>
	                        </div>
	                	</div>
              	   </div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-7">
					<div class="ulockd-faq-thumb">
						<img class="img-responsive img-whp" src="https://via.placeholder.com/670x400/2da22c?text=FAQ+Placeholder+image" alt="faq.jpg">
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection