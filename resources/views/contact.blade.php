@extends('layouts.app')

@section('page_title', 'Contact Us')

@section('head_scripts')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <style>
        #map { height:350px; margin-bottom: 20px; }
    </style>
@endsection
@section('content')

	<!-- Inner Pages Main Section -->
	<section class="ulockd-contact-page">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-fproject-title">
						<span class="ulockd-fprjct-hdr-icon"></span>
						<h2 class="text-uppercase">Contact Details</h2>
						<p>Please do not hesitate to get in touch with us for anything. Use the details below to launch enquiries, give feedback or even to say hello.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="ulockd-ohour-info style2 mb305-xsd text-center">
						<div class="ulockd-icon text-thm2"><span class="fa fa-envelope"></span></div>
						<div class="ulockd-info">
							<h3>Mail Us</h3>
							<p class="ulockd-addrss"><strong>Email:</strong> info@cvsz.co.zw</p>
						</div>
					</div>
					<div class="ulockd-ohour-info style2 text-center">
						<div class="ulockd-icon text-thm2"><span class="fa fa-whatsapp"></span></div>
						<div class="ulockd-info">
							<h3>WhatsApp</h3>
							<p class="ulockd-addrss">+263 775 471 642</p>
						</div>
					</div>
					<div class="ulockd-ohour-info style2 text-center">
						<div class="ulockd-icon text-thm2"><span class="fa fa-mobile"></span></div>
						<div class="ulockd-info">
							<h3>Mobile</h3>
							<p class="ulockd-addrss">+263 775 471 642</p>
							<p class="ulockd-addrss">+263 713 668 583</p>
							<p class="ulockd-addrss">+263 735 393 918</p>
						</div>
					</div>
					<div class="ulockd-ohour-info style2 text-center">
						<div class="ulockd-icon text-thm2"><span class="fa fa-phone"></span></div>
						<div class="ulockd-info">
							<h3>Call Us</h3>
							<p class="ulockd-addrss">+263 864 410 9886</p>
						</div>
					</div>
					<div class="ulockd-ohour-info style2 text-center">
						<div class="ulockd-icon text-thm2"><span class="fa fa-map-signs"></span></div>
						<div class="ulockd-info">
							<h3>Address</h3>
							<p>
								P O Box CY 1000<br>
								Causeway<br>
								Harare<br>
								Zimbabwe
							</p>
						</div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="ulockd-google-map">
						<div id="map"></div>
					</div>

					<div class="ulockd-contact-form style2">
						<form id="contact_form" name="contact_form" class="contact-form" method="post" novalidate="novalidate">
							@csrf
                            <div class="messages"></div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input id="name" name="name" class="form-control ulockd-form-fg required" placeholder="Your name" required="required" data-error="Name is required." type="text">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input id="email" name="email" class="form-control ulockd-form-fg required email" placeholder="Your email" required="required" data-error="Email is required." type="email">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>  
	                            <div class="col-md-4">
	                                <div class="form-group">
	                                    <input id="phone" name="phone" class="form-control ulockd-form-fg required" placeholder="Phone" required="required" data-error="Phone Number is required." type="text">
	                                    <div class="help-block with-errors"></div>
	                                </div>
	                            </div>
	                            <div class="col-md-12">
	                                <div class="form-group">
	                                    <input id="subject" name="subject" class="form-control ulockd-form-fg required" placeholder="Subject" required="required" data-error="Subject is required." type="text">
	                                    <div class="help-block with-errors"></div>
	                                </div>
	                            </div>
                                <div class="col-md-12">
		                            <div class="form-group">
		                                <textarea id="msg" name="msg" class="form-control ulockd-form-tb required" rows="8" placeholder="Your massage" required="required" data-error="Message is required."></textarea>
		                                <div class="help-block with-errors"></div>
		                            </div>
		                            <div class="form-group ulockd-contact-btn">
		                                <input id="form_botcheck" name="form_botcheck" class="form-control" value="" type="hidden">
		                                <button type="submit" class="btn btn-default btn-lg ulockd-btn-thm2" data-loading-text="Getting Few Sec...">SUBMIT</button>
		                            </div>
                                </div> 
                            </div>
                        </form>
					</div>
				</div>
				<div class="col-md-12 ulockd-mrgn1260">
				</div>
			</div>
		</div>
	</section>
@endsection

@section('footer_scripts')
<script type="text/javascript" src="js/jquery.barfiller.js"></script>
<script type="text/javascript" src="js/timepicker.js"></script>
<script type="text/javascript" src="js/validator.js"></script>
<!-- Custom script for all pages --> 
<script type="text/javascript" src="js/script.js"></script>
<script>
	/********************************
	Map
	*********************************/
	var map = L.map('map').setView([-17.785744, 31.049659], 15);
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 18,
		id: 'mapbox.streets',
		accessToken: 'pk.eyJ1Ijoicm9nZ2llIiwiYSI6ImNqeDZ4ZjU2dDA0NnozeWp5a3Zvdml1eDcifQ.uXPWg0VPZw_CEbba60AQgA'
	}).addTo(map);
	var marker = L.marker([-17.785744, 31.049659]).addTo(map);
	marker.bindPopup("<b>CVSZ</b><br>Faculty of Veterinary Science<br>University of Zimbabwe").openPopup();

	/********************************
	 *  Contact Form
	 *******************************/
	  $(function () {
      $('#contact_form').validator();
      $('#contact_form').on('submit', function (e) {
          if (!e.isDefaultPrevented()) {
            var url = "{{ url('contact') }}";
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                success: function (data)
                {
                    var messageAlert = 'alert-' + data.type;
                    var messageText = data.message;

                    var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';
                    if (messageAlert && messageText) {
                        $('#contact_form').find('.messages').html(alertBox).fadeIn('slow');
                        $('#contact_form')[0].reset();
                        setTimeout(function(){ $('.messages').fadeOut('slow') }, 6000);
                    }
                }
            });
            return false;
          }
      })
  });
</script>
@endsection