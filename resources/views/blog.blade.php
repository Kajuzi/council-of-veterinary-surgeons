@extends('layouts.app')
@section('page_title', 'Blog' )
@section('content')

	<section class="ulockd-ip-latest-news">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4">
					<article class="blog-post">
						<div class="ulockd-blog-thumb">
							<img class="img-responsive img-whp" src="uploads/blog/b1.jpg" alt="b1.jpg">
						</div>
								<div class="ulockd-blog-post-details text-left">
									<div class="ulockd-blog-post-title"><h3>Awesome Car Cleaning</h3></div>
									<ul class="list-inline">
										<li class="ulockd-post-by"><a href="#"> <i class="fa fa-user-o text-thm1"> </i> Z Lopez</a></li>
										<li class="ulockd-post-by"><a href="#"> |</a></li>
										<li class="ulockd-post-by"><a href="#"> <i class="fa fa-calendar text-thm1"> </i> 25, Jun 2017</a></li>
									</ul>
									<div class="ulockd-bpost">
										<p>Rando You consider your auto as a decent friend that has been with all of you along the street of life. You revere your four-wheeler and love to show it off to your companions. <a class="ulockd-bp-btn" href="#"> Read More...</a></p>
									</div>
								</div>
					</article>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<article class="blog-post">
						<div class="ulockd-blog-thumb">
							<img class="img-responsive img-whp" src="uploads/blog/b1.jpg" alt="b1.jpg">
						</div>
						<div class="ulockd-blog-post-details">
							<div class="ulockd-blog-post-title"><h3>Awesome Checkup</h3></div>
							<ul class="list-inline">
								<li class="ulockd-post-by"><a href="#"> <i class="fa fa-user-o text-thm1"> </i> B Willis</a></li>
								<li class="ulockd-post-by"><a href="#"> |</a></li>
								<li class="ulockd-post-by"><a href="#"> <i class="fa fa-calendar text-thm1"> </i> 25, Jun 2017</a></li>
							</ul>
							<div class="ulockd-bpost">
								<p>When you go to a Firestone Complete Auto Care store for any administration, we offer a free graciousness checkup for your vehicle.We look at your tires, liquid levels, belts, lights, wipers, battery hoses and the sky is the limit from there. <a class="ulockd-bp-btn" href="#"> Read More...</a></p>
							</div>
						</div>
					</article>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="ulockd-blog-thumb">
						<img class="img-responsive img-whp" src="uploads/blog/b1.jpg" alt="b1.jpg">
					</div>
								<div class="ulockd-blog-post-details text-left">
									<div class="ulockd-blog-post-title"><h3>Popular Automobile</h3></div>
									<ul class="list-inline">
										<li class="ulockd-post-by"><a href="#"> <i class="fa fa-user-o text-thm1"> </i> W Smith</a></li>
										<li class="ulockd-post-by"><a href="#"> |</a></li>
										<li class="ulockd-post-by"><a href="#"> <i class="fa fa-calendar text-thm1"> </i> 25, Jun 2017</a></li>
									</ul>
									<div class="ulockd-bpost">
										<p>Consistently, Popular Science respects 100 developments that are splendid, progressive, and bound to shape the future—these are the Best of What's New.  <a class="ulockd-bp-btn" href="#"> Read More...</a></p>
									</div>
								</div>
				</div>
			</div>
			<div class="row mrgn-1225">
				<div class="col-xs-12 col-sm-6 col-md-4">
					<article class="blog-post">
						<div class="ulockd-blog-thumb">
							<img class="img-responsive img-whp" src="uploads/blog/b1.jpg" alt="b1.jpg">
						</div>
								<div class="ulockd-blog-post-details text-left">
									<div class="ulockd-blog-post-title"><h3>Awesome Car Cleaning</h3></div>
									<ul class="list-inline">
										<li class="ulockd-post-by"><a href="#"> <i class="fa fa-user-o text-thm1"> </i> Z Lopez</a></li>
										<li class="ulockd-post-by"><a href="#"> |</a></li>
										<li class="ulockd-post-by"><a href="#"> <i class="fa fa-calendar text-thm1"> </i> 25, Jun 2017</a></li>
									</ul>
									<div class="ulockd-bpost">
										<p>Rando You consider your auto as a decent friend that has been with all of you along the street of life. You revere your four-wheeler and love to show it off to your companions. <a class="ulockd-bp-btn" href="#"> Read More...</a></p>
									</div>
								</div>
					</article>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<article class="blog-post">
						<div class="ulockd-blog-thumb">
							<img class="img-responsive img-whp" src="uploads/blog/b1.jpg" alt="b1.jpg">
						</div>
						<div class="ulockd-blog-post-details">
							<div class="ulockd-blog-post-title"><h3>Awesome Checkup</h3></div>
							<ul class="list-inline">
								<li class="ulockd-post-by"><a href="#"> <i class="fa fa-user-o text-thm1"> </i> B Willis</a></li>
								<li class="ulockd-post-by"><a href="#"> |</a></li>
								<li class="ulockd-post-by"><a href="#"> <i class="fa fa-calendar text-thm1"> </i> 25, Jun 2017</a></li>
							</ul>
							<div class="ulockd-bpost">
								<p>When you go to a Firestone Complete Auto Care store for any administration, we offer a free graciousness checkup for your vehicle.We look at your tires, liquid levels, belts, lights, wipers, battery hoses and the sky is the limit from there. <a class="ulockd-bp-btn" href="#"> Read More...</a></p>
							</div>
						</div>
					</article>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="ulockd-blog-thumb">
						<img class="img-responsive img-whp" src="uploads/blog/b1.jpg" alt="b1.jpg">
					</div>
								<div class="ulockd-blog-post-details text-left">
									<div class="ulockd-blog-post-title"><h3>Popular Automobile</h3></div>
									<ul class="list-inline">
										<li class="ulockd-post-by"><a href="#"> <i class="fa fa-user-o text-thm1"> </i> W Smith</a></li>
										<li class="ulockd-post-by"><a href="#"> |</a></li>
										<li class="ulockd-post-by"><a href="#"> <i class="fa fa-calendar text-thm1"> </i> 25, Jun 2017</a></li>
									</ul>
									<div class="ulockd-bpost">
										<p>Consistently, Popular Science respects 100 developments that are splendid, progressive, and bound to shape the future—these are the Best of What's New.  <a class="ulockd-bp-btn" href="#"> Read More...</a></p>
									</div>
								</div>
				</div>
			</div>
		</div>
	</section>

@endsection