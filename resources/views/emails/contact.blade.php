<strong>Name</strong>: {{ $name }} <br>
<strong>Email</strong>: {{ $email }} <br>
<strong>Phone</strong>: {{ $phone }} <br>
<strong>Message:</strong><br>
{{ $msg }}
<br>
<hr>
<sub>Sent using the contact form on the CVSZ website</sub>