@extends('layouts.app')

@section('content')
    <section>
        <div class="login-clean">
            <form method="post" action="{{ url('subscription') }}">
                {!! csrf_field() !!}
                <h2 class="sr-only">Login Form</h2>
                <div class="illustration">
                    <i class="icon ion-ios-navigate"></i>
                    <h3>
                        Pay for your supcription
                    </h3>
                </div>
                <div class="form-group">
                    <select class="form-control" name="term">
                        <option value="{{ date('Y') }}">{{ date('Y') }}</option>
                        <option value="{{ date('Y') + 1 }}">{{ date('Y') + 1 }}</option>
                    </select>
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" name="phone" value="{{ $mobile_number }}">
                    <input type="hidden" name="user_id" value="{{ $id }}">
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-block" type="submit">Proceed</button>
                </div>
            </form>
        </div>
    </section>
@endsection