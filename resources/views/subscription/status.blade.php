@extends('layouts.app')

@section('content')
    <section>
        <div class="container">
            @if(\App\Subscription::current(\Auth::user()->id))
            <p>Your subscription is current!</p>
            @else
            <p>Your subscription needs renewal</p>
            @endif
        </div>
    </section>
@endsection