@extends('layouts.app')
@section('page_title', 'Our Councilors') 
@section('content')

	<section class="ulockd-team-ip-one">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center">
					<div class="ulockd-team-title">
						<h2 class="text-uppercase">OUR <span class="green-text">COUNCILORS</span></h2>
						<p>Amet consectetur, adipisicing elit. Sequi libero iusto minima itaque esse veritatis est ullam quas.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="ulockd-l2-team-member">
						<div class="ulockd-team-thumb">
							<img class="img-responsive img-whp" src="{{ asset('uploads/pfukenyi-1.jpg') }}" alt="">
							<div class="ulockd-l2-team-mdetails">
								<div class="ulockd-member-name">Prof. Davies Mubika Pfukenyi</div>
								<div class="ulockd-team-member-post">Chairman</div>
								<ul class="list-inline ulockd-tm-sicon">
									<li><a href="#">View Profile</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="ulockd-l2-team-member">
						<div class="ulockd-team-thumb">
							<img class="img-responsive img-whp" src="{{ asset('uploads/stewart-1.jpg') }}" alt="6.jpg">
							<div class="ulockd-l2-team-mdetails">
								<div class="ulockd-member-name">Dr. R. J. E. Stewart</div>
								<div class="ulockd-team-member-post">Councilor</div>
								<ul class="list-inline ulockd-tm-sicon">
									<li><a href="#">View Profile</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="ulockd-l2-team-member">
						<div class="ulockd-team-thumb">
							<img class="img-responsive img-whp" src="{{ asset('uploads/sadziwa-1.jpg') }}" alt="7.jpg">
							<div class="ulockd-l2-team-mdetails">
								<div class="ulockd-member-name">Dr. H. R. T. Sadziwa</div>
								<div class="ulockd-team-member-post">Councilor</div>
								<ul class="list-inline ulockd-tm-sicon">
									<li><a href="#">View Profile</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="ulockd-l2-team-member">
						<div class="ulockd-team-thumb">
							<img class="img-responsive img-whp" src="{{ asset('uploads/obatula-1.jpg') }}" alt="8.jpg">
							<div class="ulockd-l2-team-mdetails">
								<div class="ulockd-member-name">Dr. Unesu H. Ushewokunze-Obatolu</div>
								<div class="ulockd-team-member-post">Chief Mechanic</div>
								<ul class="list-inline ulockd-tm-sicon">
									<li><a href="#">View Profile</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<div class="ulockd-l2-team-member">
						<div class="ulockd-team-thumb">
							<img class="img-responsive img-whp" src="{{ asset('uploads/mugabe-1.jpg') }}" alt="8.jpg">
							<div class="ulockd-l2-team-mdetails">
								<div class="ulockd-member-name">Mr. Tafadzwa R. Mugabe</div>
								<div class="ulockd-team-member-post">Councilor</div>
								<ul class="list-inline ulockd-tm-sicon">
									<li><a href="#">View Profile</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection