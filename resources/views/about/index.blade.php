@extends('layouts.app')
@section('page_title', 'About CVSZ') 
@section('content')

    <section class="ulockd-about-two">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<div class="ulockd-abouttwo-details">
						<h2>@yield('page_title')</h2>
						<p class="ulockd-about-para">The Council of Veterinary Surgeons has been in existence since 1962. It is a self-regulatory body of the veterinary profession in Zimbabwe, established under the Veterinary Surgeons Act, Chapter 27:15.</p>
                            <ul class="list-unstyled ulockd-fstabt-list">
                                <li><strong>Councillors:</strong> Four elected by the profession and two appointed by the Minister of Agriculture, Mechanization and Irrigation Development.</li>
                                <li><strong>Term of office</strong>: Three years, in effect since 1975.</li>
                                <li><strong>Council Secretariat</strong>: The full-time Registrar of Veterinary Surgeons, supported by an Office Assistant.</li>
                                <li><strong>Function of Council</strong>: Hold inquiries and other activities required for the proper regulation of the practice of the profession of veterinary surgery and medicine and the improvement of veterinary services in Zimbabwe.</li>
                                <li><strong>Council Remuneration</strong>: Registration fees and other moneys payable to the Council in terms of the Veterinary Surgeons Act [Chapter 27:15].</li>
                                <li><strong>Council membership</strong>: Three hundred and twenty-eight, as at 31st December 2016.</li>
                                <li><strong>Council Sub-committees</strong>: Board of Examiners</li>
                            </ul>
						<p class="ulockd-about-para-two">Continuing Professional Development (CPD) Accreditation</p>
					</div>
				</div>
				<div class="col-md-5">
					<div class="ulockd-about-video">
						<div class="ulockd-afv-thumb">
							<img class="img-responsive img-whp" src="{{ asset('images/kitten-about.jpg') }}" alt="kitten-about.jpg">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection