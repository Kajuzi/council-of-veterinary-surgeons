@extends('layouts.app')
@section('page_title', 'Members')
@section('content')

	<!-- Our Shop -->
	<section class="ulockd-search-results">
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<div class="row ulockd-mrgn1260 ulockd-shop-menubar ulockd-mrgn635">
						<div class="col-lg-7">
							<ul class="list-inline">
								<li><span class="flaticon-person text-thm2"></span></li>
								<li><p>Showing {{ count($members) }} results</p></li>
							</ul>
						</div>
						<div class="col-lg-5">
							<form action="{{ url( 'find' ) }}" method="post">
						    <div class="input-group ulockd-product-searchbar">
						        <input type="text" class="form-control" name="string" placeholder="Search members">
										@csrf
						        <span class="input-group-btn">
						        	<button class="btn btn-default" type="submit">Search</button>
						        </span>
						    </div><!-- /input-group -->
							</form>
						</div><!-- /.col-lg-6 -->
					</div>
					<div class="row ulockd-mrgn1260">
                    @foreach( $members as $member )
                        <div class="row search-results-row">
                            <div class="col-md-4 col-xl-4 offset-xl-0">
                                <img class="img-responsive" src="{{ asset($member->image?$member->image:'uploads/nopic.png') }}" />
                            </div>
                            <div class="col-md-4 col-xl-7">
                                <h3><a href="{{ url('member/'.$member->id) }}">{{ $member->name }}</a></h3>
                                <p class="small-grey-italics">{{ $member->qualifications }}</p>
                                <p>Specialty: {{ $member->specialty }}</p>
                                <p>URL: <a href="{{ $member->website }}" target="_blank">Visit Website</a></p>
                                <a href="{{ url('members/'.$member->id) }}" class="btn btn-lg ulockd-btn-green hvr-bounce-to-right">View Profile</a>
                            </div>
                            <div class="col-md-4 col-xl-7">
                                <div class="top-right-label">
                                   {{ $member->city }}
                                </div>
                            </div>
                        </div>
                    @endforeach                        
					</div>
				</div>
				<div class="col-lg-3">
					<div class="ulockd-all-service ulockd-mrgn1260">
						<div class="ulockd-tag-list-title">
							<h3 class="ulockd-bb-dashed"><span class="flaticon-mark text-thm1"></span> Browse by City</h3>
						</div>
						<div class="list-group">
						    <a href="#" class="list-group-item">All of Zimbabwe</a>
						    <a href="#" class="list-group-item">Harare</a>
						    <a href="#" class="list-group-item">Bulawayo</a>
						    <a href="#" class="list-group-item">Gweru</a>
						    <a href="#" class="list-group-item">Mutare</a>
						    <a href="#" class="list-group-item">Victoria Falls</a>
						    <a href="#" class="list-group-item">Kariba</a>
						    <a href="#" class="list-group-item">Kadoma</a>
						</div>
					</div>
					<div class="ulockd-ip-tag">
						<div class="ulockd-tag-list-title">
							<h3 class="ulockd-bb-dashed"><span class="flaticon-mark text-thm1"></span> Search by Specialty</h3>
						</div>
						<ul class="list-inline ulockd-tag-list-details">
							<li><a href="#">Small Animals</a></li>
							<li><a href="#">Cats</a></li>
							<li><a href="#">Horses</a></li>
							<li><a href="#">Cattle</a></li>
							<li><a href="#">Other</a></li>
						</ul>
					</div>
				</div>
				<!-- 
				<div class="col-lg-12">
					<nav class="ulockd-shop-pagination" aria-label="Page navigation">
					  <ul class="pagination">
					    <li>
					      <a href="#" aria-label="Previous">
					        <span aria-hidden="true">&laquo;</span>
					      </a>
					    </li>
					    <li><a href="#">1</a></li>
					    <li><a href="#">2</a></li>
					    <li><a href="#">3</a></li>
					    <li><a href="#">4</a></li>
					    <li><a href="#">5</a></li>
					    <li>
					      <a href="#" aria-label="Next">
					        <span aria-hidden="true">&raquo;</span>
					      </a>
					    </li>
					  </ul>
					</nav>
				</div>
				 -->
			</div>
		</div>
	</section>
@endsection
