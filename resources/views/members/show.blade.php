@extends('layouts.app')

@section('page_title', $name )

@section('head_scripts')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <style>
        #map { height:300px }
    </style>
@endsection

@section('content')
	<!-- Member Profile -->
	<section class="ulockd-shop">
		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<div class="row ulockd-mrgn1260">
						<div class="col-sm-6 col-md-6 col-lg-5 clearfix">
							<div class="ulockd-member-details-img">
                                <img class="img-responsive img-whp" src="{{ asset( $image ? $image:'uploads/nopic.png'  ) }}" alt="{{ $name }}">
                                
							</div>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-7 clearfix">
							<div class="ulockd-member-details">
								<h2 class="price">{{ $name }} </h2>
                                <p class="small-grey-italics">{{ $qualifications }}</p>
                                
                                <p><strong>Specialisation: </strong>{{ $specialty }}</p>
                                <p><strong>Phone: </strong>{{ $phone }}</p>
                                <p><strong>Email: </strong>{{ $contact_email }}</p>
                                <p><strong><a href="{{ $website }}" class="member-site-url">Visit Website</a></strong></p>
                                <p><strong>Address: </strong>{{ $address }}</p>
                                <p><strong>City/Town: </strong>{{ $city }}</p>
								<a class="btn btn-lg ulockd-btn-green hvr-bounce-to-right" href="#">Contact member</a>
								@if ( can_edit( $id ) )
								<a class="btn btn-lg ulockd-btn-green hvr-bounce-to-right" href="{{ url('members/'.$id.'/edit') }}">Edit Profile</a>
								@endif
							</div>
						</div>
						<div class="col-lg-12 ulockd-mrgn1260">
							<div class="ulockd-member-details">
								<h2 class="price">About </h2>
                                {!! $bio !!}
							</div>
							<div class="ulockd-member-details">
								<h2 class="price">Location </h2>
								<div id="map"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 ulockd-pad-zero">
					@if( sizeof($same_city) > 1 )
					<h3 class="ulockd-bb-dashed"><span class="flaticon-calendar text-thm1"></span> Also from {{ $city }}</h3>
					<div class="list-group">
							@foreach( $same_city as $scp )
								@if( $scp->id != $id )
								<a href="{{ url( 'members/' . $scp->id ) }}" class="list-group-item">
								{{ $scp->name }}
								<span class="sidebar-list-small-details">{{ $scp->address }}</span>
								</a>
								@endif
							@endforeach
					</div>
					@endif

					@if( sizeof($same_specialty) > 1 )
					<h3 class="ulockd-bb-dashed"><span class="flaticon-calendar text-thm1"></span> Also specialising in<br>{{ $specialty }}</h3>
					<div class="list-group">
							@foreach( $same_specialty as $ssp )
								@if( $ssp->id != $id )
								<a href="{{ url( 'members/' . $ssp->id ) }}" class="list-group-item">
								{{ $ssp->name }}
								<span class="sidebar-list-small-details">{{ $ssp->address }}</span>
								</a>
								@endif
							@endforeach
					</div>
					@endif
				</div>
			</div>
		</div>
	</section>
@endsection

@section('footer_scripts')
    <script>
        var map = L.map('map').setView([{{ $latlong }}], 13);
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1Ijoicm9nZ2llIiwiYSI6ImNqeDZ4ZjU2dDA0NnozeWp5a3Zvdml1eDcifQ.uXPWg0VPZw_CEbba60AQgA'
        }).addTo(map);
        var marker = L.marker([{{ $latlong }}]).addTo(map);
        marker.bindPopup("<b>{{ $name }}</b><br>{{ $address }}").openPopup();
    </script>
@endsection