@extends('layouts.app')
@section('page_title', 'Edit '.$name)
@section('head_scripts')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js" integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==" crossorigin=""></script>
    <script src="{{ asset('js/Control.Coordinates.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('js/Control.Coordinates.js') }}">
    <style>
        #map { 
            position:relative; 
            height:300px; 
            width:100% 
        }
        
        .leaflet-control-coordinates {
            background: white;
            border-radius: 4px;
            box-shadow: 0 1px 5px rgba(0, 0, 0, 0.65);
            cursor: pointer;
            padding: 2px 5px;
        }

        .leaflet-control-coordinates.hidden {
            display: none;
        }

        .leaflet-control-coordinates-lng {
            padding-left: 5px;
        }
    </style>
@endsection

@section('content')
<section class="ulockd-about-one">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <div class="">

                    <div class="">
                        <form class="general-form" method="POST" action="{{ route('members.update', $id) }}" enctype="multipart/form-data">
                            @csrf
                            {{ method_field('PATCH') }}
                                
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ? old('name') : $name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                                <div class="col-md-6">
                                    <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') ? old('username') : $username  }}" required autocomplete="username">

                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ? old('email') : $email  }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="mobile_number" class="col-md-4 col-form-label text-md-right">Mobile Number <span class='form-hint'>Will not be shown to the public</span></label>

                                <div class="col-md-6">
                                    <input id="mobile_number" type="text" class="form-control @error('mobile_number') is-invalid @enderror" name="mobile_number" value="{{ old('mobile_number') ? old('mobile_number') : $mobile_number }}" autocomplete="phone">

                                    @error('mobile_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">Business Phone</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') ? old('phone') : $phone }}" autocomplete="phone">

                                    @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="contact_email" class="col-md-4 col-form-label text-md-right">Contact Email</label>

                                <div class="col-md-6">
                                    <input id="contact_email" type="text" class="form-control @error('contact_email') is-invalid @enderror" name="contact_email" value="{{ old('contact_email') ? old('contact_email') : $contact_email }}" autocomplete="email">

                                    @error('contact_email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="bio" class="col-md-4 col-form-label text-md-right">About the practice <span class="form-hint">Feel free to use HTML tags</span></label>

                                <div class="col-md-6">
                                    <textarea id="bio" name="bio" class="form-control ulockd-form-tb" rows="8" placeholder="About the practice" data-error="Message is required.">{{ old('bio') ? old('bio') : $bio }}</textarea>    
                                    @error('bio')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="address" class="col-md-4 col-form-label text-md-right">Physical Address</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') ? old('address') : $address }}" autocomplete="address">

                                    @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">City / Town</label>

                                <div class="col-md-6">
                                    <input id="city" type="text" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') ? old('city') : $city }}" autocomplete="city">

                                    @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="latlong" class="col-md-4 col-form-label text-md-right">Location <span class="form-hint">Use map below to pick your location</span></label>

                                <div class="col-md-6">
                                    <input id="latlong" type="text" class="form-control @error('latlong') is-invalid @enderror" name="latlong" value="{{ old('latlong') ? old('latlong') : $latlong }}">

                                    @error('latlong')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div id="map"></div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="image" class="col-md-4 col-form-label text-md-right">Profile image <span class="form-hint">Max size 1MB. Please resize before uploadin</span></label>

                                <div class="col-md-6">
                                    <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" >

                                    @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn ulockd-btn-green">
                                        Update Profile
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('footer_scripts')
    <script>
		var map = L.map('map').setView([{{ $latlong }}], 8);

		L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
			maxZoom: 18,
			attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="https://carto.com/attributions">CARTO</a>'
		}).addTo(map);

		var c = new L.Control.Coordinates();
		//c.addTo(map);

        var marker = L.marker([{{ $latlong }}]).addTo(map);

		function onMapClick(e) {
			if (marker) { // check
                map.removeLayer(marker); // remove
            }
            marker = new L.Marker(e.latlng).addTo(map); // set
            $('#latlong').val(e.latlng.lat.toFixed(6) + ', ' + e.latlng.lng.toFixed(6));
		}

		map.on('click', onMapClick);
    </script>
@endsection