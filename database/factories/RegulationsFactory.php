<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Regulation;
use Faker\Generator as Faker;

$factory->define(Regulation::class, function (Faker $faker) {
    return [
        'reference' => title( $faker->words(rand(3,6), true)),
        'title' => title( $faker->words(rand(3,5), true)),
        'year' => rand(1980, date('Y')),
        'provisions' => $faker->paragraph(rand(3,6), true),
    ];
});
