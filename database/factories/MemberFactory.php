<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Member;
use Faker\Generator as Faker;

$factory->define(Member::class, function (Faker $faker) {
    $name = $faker->name;
    $email = $faker->unique()->safeEmail;
    $specialties = ['small animals', 'horses', 'cattle', 'game'];
    $city = city();
    return [
        'name' => $name,
        'email' => $email,
        'contact_email' => $email,
        'username' => slug($name),
        'address' => $faker->streetAddress,
        'mobile_number' => $faker->phoneNumber,
        'qualifications' => $faker->words(rand(1,4), true),
        'phone' => $faker->phoneNumber,
        'image' => 'uploads/profile_'.rand(1,10).'.jpg',
        'city' => $city[0],
        'latlong' => randLatlong( $city[0] ),
        'website' => $faker->url,
        'specialty' => $specialties[rand(0,3)],
        'bio' => $faker->paragraph(rand(2,4), true),
        'email_verified_at' => now(),
        'password' => bcrypt('password'),
        'remember_token' => Str::random(10),
    ];
});
