<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name'              => 'Brian Chikodze',
        'username'          => 'brian',
        'mobile_number'     => '0784123998',
        'email'             => 'registrarcvsz@gmail.com',
        'email_verified_at' => now(),
        'city'              => 'Harare',
        'is_admin'          => 1,
        'bio'               => 'The current registrar for the Council of Veterinary Surgeons of Zimbabwe',
        'latlong'           => '-17.78535, 31.04929',
        'password'          => bcrypt('password'),
        'remember_token'    => Str::random(10),
    ];
});
