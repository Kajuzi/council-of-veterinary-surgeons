<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Transaction;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    $random_time = rand(time() - 30 * 24 * 60 * 60, time());
    return [
        'user_id' => rand(2,7),
        'amount' => 120,
        'reference_number' => $random_time,
        'purpose' => 'subscription',
        'status' => 'complete',
        'completed_at' => date('Y-m-d H:m:s', $random_time),
    ];
});
