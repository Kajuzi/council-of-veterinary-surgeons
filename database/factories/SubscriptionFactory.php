<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Subscription;
use Faker\Generator as Faker;

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        'user_id' => rand(2,5),
        'term' => rand(2017, date('Y') + 1)
    ];
});
