<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouncillorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('councillors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer( 'user_id' );
            $table->string( 'position' )->default( 'councillor' );
            $table->integer( 'order' )->default( 3 );
            $table->integer( 'term_start' )->default( date('Y') + 1 );
            $table->integer( 'term_end' )->default( date('Y') + 2 );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('councillors');
    }
}
