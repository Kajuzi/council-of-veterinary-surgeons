<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory('App\User', 1)->create();
        factory('App\Member', 9)->create();
        factory('App\Subscription', 3)->create();
        factory('App\Transaction', 2)->create();
        factory('App\Regulation', 7)->create();
    }
}
