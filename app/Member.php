<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Member extends User
{
    protected $table = 'users';

    /**
     * Update a member's profile information
     *
     * @param integer $id
     * @param MemberUpdateRequest $request
     * @return void
     */
    public static function muto(int $id, MemberUpdateRequest $request)
    {
        $member = Member::find($id);
        $data = $request->all();
        if( $request->hasFile( 'image' ) )
            $data['image'] = Member::upload_image( $request->image );
        $member->update( $data );
        $member->save();
        return $member;
    }

    /**
     * Find members in a specific city
     *
     * @param string $city
     * @return object
     */
    public static function by_city( string $city )
    {
        $members = Member::where( 'city', $city )->get();
        return $members;
    }

    /**
     * Find members in a specific specialty
     *
     * @param string $specialty
     * @return object
     */
    public static function by_specialty( string $specialty )
    {
        $members = Member::where( 'specialty', $specialty )->get();
        return $members;
    }

    /**
     * Upload an image and return its path
     *
     * @param UploadedFile $image
     * @return string
     */
    public static function upload_image( UploadedFile $image )
    {
        return 'storage/' . $image->store( 'uploads', 'public' );
    }


    public static function search( string $string )
    {
        return Member::where( 'name', 'like', "%$string%" )
                    ->orWhere( 'bio', 'like', "%$string%" )
                    ->orWhere( 'specialty', 'like', "%$string%" )
                    ->orWhere( 'city', 'like', "%$string%" )
                    ->get();
    }
}
