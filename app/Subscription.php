<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    public static function nu( $data )
    {
        $sub = new Subscription();
        $sub->user_id = $data['user_id'];
        $sub->term = $data['term'];
        $sub->save();
        return $sub;
    }

    public static function current( $id )
    {
        $current = Subscription::where('user_id', $id )
                                ->where('term', date('Y'))
                                ->get();
        if(sizeof($current)){
            return $current;
        } else {
            return false;
        }
    }
}
