<?php

namespace App;

use \Paynow\Payments\Paynow;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public static function initPaynowOther( $data )
    {
        $pnId = env('PAYNOW_INTEGRATION_ID');
        $pnKey = env('PAYNOW_INTEGRATION_KEY');
        $pollUrl = env('PAYNOW_POLL_URL', 'http://panlista.com/demo/cvsz/paynow_poll');
        $rUrl = env('PAYNOW_REDIRECT_URL', 'http://panlista.com/demo/cvsz/paynow_return');

        $paynow = new Paynow($pnId, $pnKey);

        $payment = $paynow->createPayment(now(), \Auth::user()->email);

        $payment->add('12 Months Subscription', 120)
                ->setDescription("Membership fees for one year");

        // Initiate a Payment 
        $response = $paynow->send($payment);

        if (!$response->success) {
            return ['success' => FALSE, 'message' => $response->error];
        } else {
            return [ 'success' => TRUE, 'payment' => $payment, 'response' => $response ];
        }
    }

    public static function completePaynow( $data )
    {
        $transaction = new Transaction ();

        $transaction->user_id = Auth::user()->id;
        $transaction->amount = 120;
        $transaction->reference_number = now();
        $transaction->purpose = 'subscription';
        $transaction->status = 'complete';

        $transaction->save();

        return $transaction;
    }


    public static function ese( )
    {
        return Transaction::where('user_id', \Auth::user()->id)->get();
    }

    public static function paynowEcocash( $data )
    {
        
    }

}
