<?php

namespace App\Http\Controllers;

use \Mail;
use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;

class PagesController extends Controller
{
    public static function sendmail(ContactRequest $r)
    {
        Mail::send('emails.contact', $r->all(), function($message) use ($r)
        {
            $message->to(config('mail.from.address'))
                ->from($r->email)
                ->subject('[Webform] '.$r->subject);
        });

        return $r;
    }

    public static function about()
    {
        return view('about.index');
    }
}
