<?php

if (! function_exists('pub')) {
    /**
     * Generate an asset path for the resources in the public folder.
     *
     * @param  string  $path
     * @param  bool    $secure
     * @return string
     */
    function pub($path)
    {
        return config('app.url')."/public/{$path}";
    }
}

if (! function_exists('prod')) {
    /**
     * Generate an asset path for the resources in the product folder.
     *
     * @param  string  $path
     * @param  bool    $secure
     * @return string
     */
    function prod($path)
    {
        return config('app.baseurl')."/storage/products/{$path}";
    }
}

if (! function_exists('title')) {
    /**
     * Generate product title from string.
     *
     * @param  string  $path
     * @return string
     */
    function title($string)
    {
        $string = ( strpos( $string, ' (' )) ? substr( $string, 0, $l ) : $string ;
        $pieces = preg_split( "/[\s,_-]+/", $string );
        foreach ( $pieces as $key => $piece ) {
            if ( preg_match('/[0-9]/', $piece) || strlen( $piece ) < 3 ) {
                unset( $pieces[$key] );
            }
        }

        if ( $pieces )
            $title = implode(' ', $pieces);
        else
            return 'Untitled Image';

        $title = ucwords($title);
        return $title;
    }
}

if (! function_exists('slug')) {
    /**
     * Generate slug from string.
     *
     * @param  string  $string
     * @return string
     */
    function slug($string)
    {
        $string = strtolower($string);
        return str_replace([' ', '.', '_'], '-', $string);
    }
}

if (! function_exists('randLatlong')) {
    /**
     * Generate random location coodinates
     * @param 
     * @return string
     */
    function randLatlong( $city = 'Harare' )
    {
        $city = city( $city );
        $lat = $city[1] + rand(-0.0100, 0.0100);
        $long = $city[2] + rand(-0.0100, 0.0100);
        return $lat . ',' . $long;
    }
}

if (! function_exists('title_bg')) {
    /**
     * Return the title background of a page. Default to the site's default
     * @param 
     * @return string
     */
    function title_bg($view)
    {
        $rpath = \Str::after(url()->current(), url('/').'/');
        $img_file = public_path().'/images/headers/'.$rpath.'.jpg';
        $bg = file_exists($img_file)?$rpath.'.jpg':'default-header-bg.jpg';
        return asset("images/headers/$bg");
    }
}

if (! function_exists('has_header')) {
    /**
     * Determine if the page should display a header or not
     * @param 
     * @return bool
     */
    function has_header()
    {
        if (url()->current() == url('/'))
            return false;
        $exempted = ['home', 'dashboard'];
        $rpath = \Str::after(url()->current(), url('/').'/');
        return !(in_array($rpath, $exempted) || $rpath == '');
    }
}


if (! function_exists('can_edit')) {
    /**
     * Determine if the page should display a header or not
     * @param int id
     * @return bool
     */
    function can_edit( $id )
    {
        if ( \Auth::check() && ( \Auth::user()->is_admin || \Auth::user()->id == $id ) )
            return true;
        else 
            return false;
    }
}

if (! function_exists('city')) {
    /**
     * Determine if the page should display a header or not
     * @param string city
     * @return bool
     */
    function city( $city = '' )
    {
        $cities = [
            'Harare' => ['Harare', -17.826840, 31.049767],
            'Gweru' => ['Gweru', -19.455700, 29.817100],
            'Mutare' => ['Mutare', -18.982800, 32.644200],
            'Bulawayo' => ['Bulawayo', -20.143500, 28.563400],
            'Masvingo' => ['Masvingo', -20.074900, 30.822500]
        ];
        if ( $city && isset( $cities[ $city ] ) )
            return $cities[ $city ];
        else
            return $cities[ array_rand( $cities ) ];

    }
}